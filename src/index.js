import React from 'react';
import ReactDOM from 'react-dom';

// Mock Response JSON
// データはカテゴリをキーにソートされている想定
const PRODUCTS = [
    {category: "Sporting Goods", price: "$49.99", stocked: true, name: "Football"},
    {category: "Sporting Goods", price: "$9.99", stocked: true, name: "Baseball"},
    {category: "Sporting Goods", price: "$29.99", stocked: false, name: "Basketball"},
    {category: "Electronics", price: "$99.99", stocked: true, name: "iPod Touch"},
    {category: "Electronics", price: "$399.99", stocked: false, name: "iPhone 5"},
    {category: "Electronics", price: "$199.99", stocked: true, name: "Nexus 7"}
];


/**
 * 製品の行
 */

class ProductRow extends React.Component {
    render() {
        const product = this.props.product;
        const name = product.stocked ?
            product.name :
            <span style={{color: 'red'}}>
                {product.name}
            </span>;
        return (
            <div className="siimple-table-row">
                <div className="siimple-table-cell">{name}</div>
                <div className="siimple-table-cell">{product.price}</div>
            </div>
        )
    }
}

/**
 * 製品のカテゴリ
 */
class ProductCategoryRow extends React.Component {
    render() {
        const category = this.props.category;
        return (
            <div className="siimple-table-row">
                <div className="siimple-table-cell">
                    <div className="siimple--color-primary">{category}</div>
                </div>
                <div className="siimple-table-cell"></div>
            </div>
        )
    }
}

/**
 * 製品とカテゴリを表示する領域
 */
class ProductTable extends React.Component {
    render() {
        const rows = [];
        let lastCategory = null;

        const filterText = this.props.filterText;
        const inStockOnly = this.props.inStockOnly;

        for (const product of this.props.products) {
            // 前方一致しなかったら。
            if (product.name.indexOf(filterText) === -1) {
                continue;
            }

            // 在庫切れ
            if (inStockOnly && !product.stocked) {
                continue;
            }
            if (product.category !== lastCategory) {
                rows.push(
                    <ProductCategoryRow
                        category={product.category}
                        key={product.category}
                    />
                )
            }
            rows.push(
                <ProductRow
                    product={product}
                    key={product.name}
                />
            );
            lastCategory = product.category;
        }

        return (
            <div className="siimple-table">
                <div className="siimple-table-header">
                    <div className="siimple-table-row">
                        <div className="siimple-table-cell">Name</div>
                        <div className="siimple-table-cell">Price</div>
                    </div>
                </div>
                <div className="siimple-table-body">
                    {rows}
                </div>
            </div>
        )
    }
}

/**
 * 検索フィールド
 */
class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
        this.handleInStockChange = this.handleInStockChange.bind(this);
    }

    handleFilterTextChange(e) {
        this.props.onFilterTextChange(e.target.value);
    }

    handleInStockChange(e) {
        this.props.onInStockChange(e.target.checked)
    }

    render() {
        return (
            <div className="siimple-form">
                <div className="siimple-form-field">
                    <input
                        type="text"
                        className="siimple-input"
                        placeholder="Search..."
                        value={this.props.filterText}
                        onChange={this.handleFilterTextChange}
                    />
                </div>
                <div className="siimple-form-field">
                    <div className="siimple-checkbox">
                        <input
                            type="checkbox"
                            checked={this.props.inStockOnly}
                            onChange={this.handleInStockChange}
                            id="myCheckbox"
                        />
                        <label htmlFor="myCheckbox"></label>
                    </div>
                    <label className="siimple-label">Only show products in stock</label>
                </div>
            </div>
        )
    }
}

/**
 * 全ての親
 */
class FilterableProductTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filterText: '',
            inStockOnly: false,
        };


        this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
        this.handleInStockChange = this.handleInStockChange.bind(this);
    }

    handleFilterTextChange(filterText) {
        this.setState({
            filterText: filterText
        });
    }

    handleInStockChange(inStockOnly) {
        this.setState({
            inStockOnly: inStockOnly
        });
    }

    render() {
        const style = {
            textAlign:"center",
        };
        return (
            <div className="siimple-grid">
                <div className="siimple-grid-row">
                    <div className="siimple-grid-col siimple-grid-col--3"></div>
                    <div className="siimple-grid-col siimple-grid-col--6" style={style}>
                        <SearchBar
                            filterText={this.state.filterText}
                            inStockOnly={this.state.inStockOnly}
                            onFilterTextChange={this.handleFilterTextChange}
                            onInStockChange={this.handleInStockChange}
                        />
                    </div>
                    <div className="siimple-grid-col siimple-grid-col--3"></div>
                </div>
                <div className="siimple-grid-row">
                    <div className="siimple-grid-col siimple-grid-col--3"></div>
                    <div className="siimple-grid-col siimple-grid-col--6">
                        <ProductTable
                            products={this.props.products}
                            filterText={this.state.filterText}
                            inStockOnly={this.state.inStockOnly}
                        />
                    </div>
                    <div className="siimple-grid-col siimple-grid-col--3"></div>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <FilterableProductTable products={PRODUCTS}/>,
    document.getElementById('container'),
);