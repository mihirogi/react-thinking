## 概要

これ:  
https://reactjs.org/docs/thinking-in-react.html

## 環境

導入

```
npm install -g create-react-app
create-react-app project-name
npm install
```

Webstorm
* ファイル -> 設定 -> JavaScript -> JavaScript言語バージョン -> ReactJSX
    + これを設定しないと、シンタックスエラーがでる 

### コンポーネントに分離

* FilterableProduct
    * SearchBar
    * ProductTable
        * ProductCategoryRow
        * ProductRow
        
        
### 状態か否か

ありうるデータのパターン

* APIから受け取る製品リスト
* ユーザがテキストボックスに入力する検索ワード
* チェックボックスの値
* フィルタリングされた後の製品リスト

データパターンに対して、状態であるか否か確認

1. 親コンポーネントから`props`を通して渡されるのであれば、状態ではない
2. 時間が経過しても変化しないのであれば、状態ではない
3. コンポーネント内の他のコンポーネントから計算が可能であれば、状態ではない

> * APIから受け取る製品リスト

`props`を通してコンポーネントにわたっていくので、状態ではない

> * ユーザがテキストボックスに入力する検索ワード
> * チェックボックスの値

時間とともに変化して、他のコンポーネントからは計算ができないので状態である

> * フィルタリングされた後の製品リスト

製品リストと検索テキストとチェックボックスの値から計算ができるので、状態ではない


状態は、とりあえず以下2つ  
* ユーザがテキストボックスに入力する検索ワード
* チェックボックスの値

## CSSフレームワークでちょっとおしゃれにしてみる

使うCSSフレームワークはいつもの :   
https://docs.siimple.xyz/grid/overview.html

* [/] 検索部分をおしゃれにしてみる
* [] テーブル部分をおしゃれにしてみる

### 作戦

* FilterableProductTableでgirdは作る
* FilterableProductTableに、`state`を集める
* SearchBarに同居してるテキストボックスとチェックボックスを別々のコンポーネントにする


### reactにgrid-layoutってどうやるんだよ・・・。

react-grid-layoutっていうライブラリがあるらしい。  
ただ、siimple使いたいので、、、JSXでうまく書くしかないのかな...  
* row-columnとコンポーネントの切り出しがすごい難しそう  
* 親コンポーネントの`state`が肥大化しそう
* gird用のコンポーネントを切り出して見る...?
* gridレイアウトはreact-grid-layoutに任せて、その他はCSSフレームワーク対応がいいんじゃないこれ・・・
* grid-layoutとformって相性悪くない...?
    + div入れ子になるし....
    + CSSフレームワークだと、form用のclass用意されてるからそれ使うから問題ないのかも


